<?php
/**
 * Created by PhpStorm.
 * User: herizo
 * Date: 11/6/18
 * Time: 3:01 PM
 */

namespace App\Controller;


use App\Entity\Post;
use App\Formater\HtmlFormater;
use App\Formater\TextFormater;
use App\Formater\XMLFormater;
use App\FormType\PostType;
use App\Observers\PostObserver;
use App\Util\DBWriter;
use App\Util\FileWriter;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SamplesController extends AbstractController
{
    protected $registryInterface;
    public function __construct(RegistryInterface $registryInterface)
    {
        $this->registryInterface = $registryInterface;
    }

    /**
     * @Route("/hello", name="samples_hello", methods={"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function helloWorldAction(Request $request)
    {
        return $this->render("helloWorld.html.twig");
    }

    /**
     * @Route("/form-test", name="samples_form_test", methods={"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showFormAction(Request $request)
    {
        return $this->render("formTest.html.twig");
    }

    /**
     * @Route("/add-post", name="samples_add_post")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPost(Request $request)
    {
        $post = new Post();
        $formPost = $this->createForm(PostType::class, $post);
        $formPost->handleRequest($request);
        if ( $formPost->isSubmitted() && $formPost->isValid() ) {
            $post = $formPost->getData();
            //db writer
            $writer = new DBWriter(new TextFormater(), $this->registryInterface);
            $writer->write($post);

            //File writer xml
            $writer = new FileWriter(new XMLFormater(), 'file.xml');
            $writer->write($post);

            $writer = new FileWriter(new HtmlFormater(), 'file.html');
            $writer->write($post);
        }

        return $this->render("formPost.html.twig", [
            'formPost' => $formPost->createView()
        ]);
    }

    /**
     * @Route("/click-me", name="click_me")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clickMe()
    {
        return $this->render('clickMe.html.twig');
    }
}