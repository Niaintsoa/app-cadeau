<?php
/**
 * Created by PhpStorm.
 * User: herizo
 * Date: 11/9/18
 * Time: 1:40 PM
 */

namespace App\FormType;

use App\Entity\Post;
use Symfony\Component\Form\Test\TypeTestCase;

class PostTypeTest extends TypeTestCase
{
    public function testSubmitData()
    {
        $formData = array(
            'title' => 'Post 1',
            'content' => null
        );
        $post = new Post();
        $form = $this->factory->create(PostType::class, $post);
        //populate a post object with the data in formData
        $postWithFormData = new Post();
        $postWithFormData->setTitle($formData['title']);
        $postWithFormData->setContent($formData['content']);
        //submit the data to the form directly
        $form->submit($formData);
        $this->assertTrue($form->isSynchronized());
        //check $post was modified correctly after the submit
        $this->assertEquals($postWithFormData, $post);

        $view = $form->createView();
        //check if all the widget are displayed correctly
        $children = $view->children;
        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}